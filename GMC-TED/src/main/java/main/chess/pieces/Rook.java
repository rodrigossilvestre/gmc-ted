package main.chess.pieces;

import main.boardgame.Board;
import main.chess.ChessPiece;
import main.chess.Color;

public class Rook extends ChessPiece {

	public Rook(Board board, Color color) {
		super(board, color);
	}

	@Override
	public String toString() {
		return "R";
	}

}
